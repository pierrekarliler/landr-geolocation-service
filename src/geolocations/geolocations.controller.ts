import {
    Controller,
    Get,
    NotFoundException,
    UnprocessableEntityException,
    InternalServerErrorException,
    Post,
    Body,

} from '@nestjs/common';
import { InvalidIpsError } from '../common/errors/InvalidIpsError';
import { GeolocationsNotFoundError } from '../common/errors/GeolocationsNotFoundError';
import { GeolocationsService } from './geolocations.service';
import { IpAddress } from '../common/decorators/ipAddress.decorator';

@Controller('geolocations')
export class GeolocationsController {

    constructor(
        private _geolocationsService: GeolocationsService
    ) { }

    /**
     * Returns the geolocation of the request caller.
     * 
     * @Method GET
     * @param {string} ip The ip of the caller
     * @returns {HttpResponse}
     */
    @Get()
    get(
        @IpAddress() ip
    ) {
        try {

            return this._geolocationsService.get(ip)
        } catch (error) {

            if (error instanceof InvalidIpsError)
                throw new UnprocessableEntityException(error.message)

            if (error instanceof GeolocationsNotFoundError)
                throw new NotFoundException(error.message)

            // Log the server error for trace
            console.log(error)
            throw new InternalServerErrorException()
        }
    }

    /**
     * Returns the geolocation of all the IPs in
     *  a given array.
     * 
     * @Method POST
     * @param {{ ips: string[] }} { ips } The array of ip to get the geolocations
     * @returns
     * @memberof GeolocationsController
     */
    @Post()
    getMultiple(
        @Body() { ips }: { ips: string[] }
    ) {
        try {
            return this._geolocationsService.getMultiple(ips)
        } catch (error) {

            if (error instanceof InvalidIpsError)
                throw new UnprocessableEntityException(error.message)

            if (error instanceof GeolocationsNotFoundError)
                throw new NotFoundException(error.message)

            // Log the server error for trace
            console.log(error)
            throw new InternalServerErrorException()
        }
    }
}
