import { Module } from '@nestjs/common';
import { GeolocationsController } from './geolocations.controller';
import { GeolocationsService } from './geolocations.service';
import { GeolocationsRepository } from './geolocations.repository';
import { ConfigModule } from 'nestjs-dotenv';

@Module({
    imports: [
        ConfigModule.forRoot('environment/.env'),
        GeolocationsRepository
    ],
    controllers: [GeolocationsController],
    providers: [
        GeolocationsRepository,
        GeolocationsService
    ]
})
export class GeolocationsModule { }
