import { Test, TestingModule } from '@nestjs/testing';
import { GeolocationsService } from './geolocations.service';
import { GeolocationsRepository } from './geolocations.repository';
import { InvalidIpsError } from '../common/errors/InvalidIpsError';
import { GeolocationsNotFoundError } from '../common/errors/GeolocationsNotFoundError';

const GEOLOCATION_DATA = {
    "continent": {
        "code": "NA",
        "geonameId": 6255149,
        "names": {
            "de": "Nordamerika",
            "en": "North America",
            "es": "Norteamérica",
            "fr": "Amérique du Nord",
            "ja": "北アメリカ",
            "pt-BR": "América do Norte",
            "ru": "Северная Америка",
            "zh-CN": "北美洲"
        }
    },
    "country": {
        "geonameId": 6251999,
        "isoCode": "CA",
        "names": {
            "de": "Kanada",
            "en": "Canada",
            "es": "Canadá",
            "fr": "Canada",
            "ja": "カナダ",
            "pt-BR": "Canadá",
            "ru": "Канада",
            "zh-CN": "加拿大"
        }
    },
    "maxmind": {},
    "registeredCountry": {
        "geonameId": 6251999,
        "isoCode": "CA",
        "names": {
            "de": "Kanada",
            "en": "Canada",
            "es": "Canadá",
            "fr": "Canada",
            "ja": "カナダ",
            "pt-BR": "Canadá",
            "ru": "Канада",
            "zh-CN": "加拿大"
        },
        "isInEuropeanUnion": false
    },
    "representedCountry": {},
    "traits": {
        "ipAddress": "74.58.70.209",
        "isAnonymous": false,
        "isAnonymousProxy": false,
        "isAnonymousVpn": false,
        "isHostingProvider": false,
        "isLegitimateProxy": false,
        "isPublicProxy": false,
        "isSatelliteProvider": false,
        "isTorExitNode": false
    }
}

class GeolocationsRepositoryMock {
    init() { }
    get(ip: string) {
        return ip === '74.58.70.209' ? GEOLOCATION_DATA : null
    }
    getMultiple(ips: string[]) {
        return []
    }
}

describe('GeolocationsService', () => {
    const geolocationsRepositoryProvider = {
        provide: GeolocationsRepository,
        useClass: GeolocationsRepositoryMock
    }

    let geolocationsService: GeolocationsService;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [geolocationsRepositoryProvider, GeolocationsService],
        }).compile();

        geolocationsService = module.get<GeolocationsService>(GeolocationsService);
    });

    it('should be defined', () => {
        expect(geolocationsService).toBeDefined();
    });

    describe('get', () => {
        it('should get the ip\'s geolocation', () => {
            const expectedGeolocation = GEOLOCATION_DATA
            const geolocation = geolocationsService.get('74.58.70.209');

            expect(geolocation).toEqual(expectedGeolocation);
        });

        it('should throw a default InvalidIpsError exception', () => {
            const invalidIp = ''
            const invalidIpsError = new InvalidIpsError()
            try {
                geolocationsService.get(invalidIp);
            } catch (err) {
                expect(err).toEqual(invalidIpsError)
            }
        })

        it('should throw a custom InvalidIpsError exception', () => {
            const invalidIp = '74.58.70'
            const invalidIpsError = new InvalidIpsError([invalidIp])
            try {
                geolocationsService.get(invalidIp);
            } catch (err) {
                expect(err).toEqual(invalidIpsError)
            }
        })

        it('should throw a GeolocationsNotFoundError exception', () => {
            const notFoundIp = '74.58.70.200'
            const geolocationsNotFoundError = new GeolocationsNotFoundError([notFoundIp])
            try {
                geolocationsService.get(notFoundIp);
            } catch (err) {
                expect(err).toEqual(geolocationsNotFoundError)
            }
        })
    })

    describe('getMultiple', () => {
        it('should get the ips\' geolocation', () => {
            const expectedGeolocation = [GEOLOCATION_DATA, GEOLOCATION_DATA]
            const geolocation = geolocationsService.getMultiple(['74.58.70.209', '74.58.70.209']);

            expect(geolocation).toEqual(expectedGeolocation);
        })

        it('should throw a default InvalidIpsError exception', () => {
            const invalidIps = []
            const invalidIpsError = new InvalidIpsError()
            try {
                geolocationsService.getMultiple(invalidIps);
            } catch (err) {
                expect(err).toEqual(invalidIpsError)
            }
        })

        it('should throw a custom InvalidIpsError exception', () => {
            const invalidIps = ['74.58.70', '1234.0', 'This is not an ip']
            const invalidIpsError = new InvalidIpsError(invalidIps)
            try {
                geolocationsService.getMultiple(invalidIps);
            } catch (err) {
                expect(err).toEqual(invalidIpsError)
            }
        })

        it('should throw a GeolocationsNotFoundError exception', () => {
            const notFoundIps = ['74.58.70.200', '74.58.70.201', '74.58.70.202']
            const geolocationsNotFoundError = new GeolocationsNotFoundError(notFoundIps)
            try {
                geolocationsService.getMultiple(notFoundIps);
            } catch (err) {
                expect(err).toEqual(geolocationsNotFoundError)
            }
        })
    })
})
