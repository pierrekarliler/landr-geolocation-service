/* istanbul ignore file */
import { Injectable } from '@nestjs/common';
import { Reader, Country } from '@maxmind/geoip2-node';
import { AddressNotFoundError } from '@maxmind/geoip2-node/dist/src/errors'
import { ConfigService, ConfigInjection } from 'nestjs-dotenv';
import ReaderModel from '@maxmind/geoip2-node/dist/src/readerModel';

@Injectable()
export class GeolocationsRepository {

    private readerModel: ReaderModel

    constructor(
        @ConfigInjection() private readonly _configService: ConfigService
    ) { }

    /**
     * Loads the database file and inits the reader
     */
    async init() {
        this.readerModel = await Reader.open(this._configService.get('DATABASE_PATH'))
            .catch(err => {
                // Log the error and ensure the server process stops
                console.error(err.toString())
                process.exit(0)
            }) as ReaderModel
    }

    /**
     * Returns the Country of a given ip.
     * @param ip The ip to use
     */
    get(ip: string): Country | null {
        let country: Country | null
        try {
            country = this.readerModel.country(ip)
        } catch (err) {
            if (err instanceof AddressNotFoundError)
                country = null
            else throw err
        }
        return country
    }
}
