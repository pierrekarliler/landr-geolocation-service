import { Test, TestingModule } from '@nestjs/testing';
import { GeolocationsService } from './geolocations.service';
import { InvalidIpsError } from '../common/errors/InvalidIpsError';
import { GeolocationsNotFoundError } from '../common/errors/GeolocationsNotFoundError';
import { GeolocationsController } from './geolocations.controller';
import { UnprocessableEntityException, NotFoundException, InternalServerErrorException } from '@nestjs/common';
import { GeolocationsRepository } from './geolocations.repository';
import { Country } from '@maxmind/geoip2-node';

const GEOLOCATION_DATA = {
    "continent": {
        "code": "NA",
        "geonameId": 6255149,
        "names": {
            "de": "Nordamerika",
            "en": "North America",
            "es": "Norteamérica",
            "fr": "Amérique du Nord",
            "ja": "北アメリカ",
            "pt-BR": "América do Norte",
            "ru": "Северная Америка",
            "zh-CN": "北美洲"
        }
    },
    "country": {
        "geonameId": 6251999,
        "isoCode": "CA",
        "names": {
            "de": "Kanada",
            "en": "Canada",
            "es": "Canadá",
            "fr": "Canada",
            "ja": "カナダ",
            "pt-BR": "Canadá",
            "ru": "Канада",
            "zh-CN": "加拿大"
        }
    },
    "maxmind": {},
    "registeredCountry": {
        "geonameId": 6251999,
        "isoCode": "CA",
        "names": {
            "de": "Kanada",
            "en": "Canada",
            "es": "Canadá",
            "fr": "Canada",
            "ja": "カナダ",
            "pt-BR": "Canadá",
            "ru": "Канада",
            "zh-CN": "加拿大"
        },
        "isInEuropeanUnion": false
    },
    "representedCountry": {},
    "traits": {
        "ipAddress": "74.58.70.209",
        "isAnonymous": false,
        "isAnonymousProxy": false,
        "isAnonymousVpn": false,
        "isHostingProvider": false,
        "isLegitimateProxy": false,
        "isPublicProxy": false,
        "isSatelliteProvider": false,
        "isTorExitNode": false
    }
}

class GeolocationsRepositoryMock {
    init() { }
    get(ip: string) {
        return ip === '74.58.70.209' ? GEOLOCATION_DATA : null
    }
    getMultiple(ips: string[]) {
        return []
    }
}

describe('GeolocationsController', () => {
    const geolocationsRepositoryProvider = {
        provide: GeolocationsRepository,
        useClass: GeolocationsRepositoryMock
    }
    let geolocationsService: GeolocationsService
    let geolocationsController: GeolocationsController

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [GeolocationsController],
            providers: [geolocationsRepositoryProvider, GeolocationsService],
        }).compile();

        geolocationsService = module.get<GeolocationsService>(GeolocationsService)
        geolocationsController = module.get<GeolocationsController>(GeolocationsController)
    });

    it('should be defined', () => {
        expect(geolocationsController).toBeDefined();
    });

    describe('get', () => {
        it('should get the ip\'s geolocation', () => {
            const expectedResult = GEOLOCATION_DATA as Country
            jest.spyOn(geolocationsService, 'get').mockImplementation(() => expectedResult)

            const geolocation = geolocationsController.get('74.58.70.209')
            expect(geolocation).toEqual(expectedResult);
        })

        it('should throw a default UnprocessableEntityException exception', () => {
            const invalidIp = ''
            const invalidIpsError = new InvalidIpsError()
            jest.spyOn(geolocationsService, 'get').mockImplementation(() => {
                throw new InvalidIpsError()
            })

            try {
                geolocationsController.get(invalidIp)
            } catch (err) {
                expect(err.status).toBe(new UnprocessableEntityException().getStatus())
                expect(err.message.message).toBe(invalidIpsError.message)
            }
        })

        it('should throw a custom UnprocessableEntityException exception', () => {
            const invalidIp = '74.58.70'
            const invalidIpsError = new InvalidIpsError([invalidIp])
            jest.spyOn(geolocationsService, 'get').mockImplementation(() => {
                throw invalidIpsError
            })

            try {
                geolocationsController.get(invalidIp)
            } catch (err) {
                expect(err.status).toBe(new UnprocessableEntityException().getStatus())
                expect(err.message.message).toBe(invalidIpsError.message)
            }
        })

        it('should throw a NotFoundException exception', () => {
            const notFoundIp = '74.58.70.200'
            const notFoundExceptionExcepted = new GeolocationsNotFoundError([notFoundIp])
            jest.spyOn(geolocationsService, 'get').mockImplementation(() => {
                throw notFoundExceptionExcepted
            })

            try {
                geolocationsController.get(notFoundIp)
            } catch (err) {
                expect(err.status).toBe(new NotFoundException().getStatus())
                expect(err.message.message).toBe(notFoundExceptionExcepted.message)
            }
        })

        it('should throw an InternalServerErrorException exception', () => {
            const notFoundIp = '74.58.70.200'
            jest.spyOn(geolocationsService, 'get').mockImplementation(() => {
                throw new Error()
            })

            try {
                geolocationsController.get(notFoundIp)
            } catch (err) {
                expect(err.message).toStrictEqual(Object(new InternalServerErrorException().message))
            }
        })
    })

    describe('getMultiple', () => {
        it('should get the ips\' geolocation', () => {
            const expectedGeolocations = [GEOLOCATION_DATA, GEOLOCATION_DATA] as Country[]
            jest.spyOn(geolocationsService, 'getMultiple').mockImplementation(() => expectedGeolocations)

            const geolocations = geolocationsController.getMultiple({
                ips: ['74.58.70.209', '74.58.70.209']
            })
            expect(geolocations).toEqual(expectedGeolocations);
        })

        it('should throw a default UnprocessableEntityException exception', () => {
            const invalidIps = []
            const invalidIpsError = new InvalidIpsError(invalidIps)
            jest.spyOn(geolocationsService, 'getMultiple').mockImplementation(() => {
                throw invalidIpsError
            })

            try {
                geolocationsController.getMultiple({
                    ips: invalidIps
                })
            } catch (err) {
                expect(err.status).toBe(new UnprocessableEntityException().getStatus())
                expect(err.message.message).toBe(invalidIpsError.message)
            }
        })

        it('should throw a custom UnprocessableEntityException exception', () => {
            const invalidIps = ['74.58.70', '1234.0', 'This is not an ip']
            const invalidIpsError = new InvalidIpsError(invalidIps)
            jest.spyOn(geolocationsService, 'getMultiple').mockImplementation(() => {
                throw invalidIpsError
            })

            try {
                geolocationsController.getMultiple({
                    ips: invalidIps
                })
            } catch (err) {
                expect(err.status).toBe(new UnprocessableEntityException().getStatus())
                expect(err.message.message).toBe(invalidIpsError.message)
            }
        })

        it('should throw a NotFoundException exception', () => {
            const notFoundIps = ['74.58.70.200', '74.58.70.201', '74.58.70.202']
            const geolocationsNotFoundError = new GeolocationsNotFoundError(notFoundIps)
            jest.spyOn(geolocationsService, 'getMultiple').mockImplementation(() => {
                throw geolocationsNotFoundError
            })

            try {
                geolocationsController.getMultiple({
                    ips: notFoundIps
                })
            } catch (err) {
                expect(err.status).toBe(new NotFoundException().getStatus())
                expect(err.message.message).toBe(geolocationsNotFoundError.message)
            }
        })

        it('should throw an InternalServerErrorException exception', () => {
            const notFoundIps = ['74.58.70.200', '74.58.70.201', '74.58.70.202']
            jest.spyOn(geolocationsService, 'getMultiple').mockImplementation(() => {
                throw new Error()
            })

            try {
                geolocationsController.getMultiple({
                    ips: notFoundIps
                })
            } catch (err) {
                expect(err.message).toStrictEqual(Object(new InternalServerErrorException().message))
            }
        })
    })
})
