import { Injectable } from '@nestjs/common';
import { GeolocationsRepository } from './geolocations.repository';
import * as Joi from '@hapi/joi'
import { InvalidIpsError } from '../common/errors/InvalidIpsError';
import { GeolocationsNotFoundError } from '../common/errors/GeolocationsNotFoundError';
import { Country } from '@maxmind/geoip2-node';

@Injectable()
export class GeolocationsService {

    constructor(
        private _geolocationsRepository: GeolocationsRepository
    ) {
        _geolocationsRepository.init()
    }

    /**
     * Returns the geolocation of a given IP.
     *
     * @param {string} ip The ip to get the geolocation from
     * @return {Country}
     */
    get(ip: string) {

        if (!ip) throw new InvalidIpsError()

        // Ensure the provided IP is in a valid format
        if (Joi.string().ip().validate(ip).error) throw new InvalidIpsError([ip])

        const geolocation = this._geolocationsRepository.get(ip)

        if (!geolocation) throw new GeolocationsNotFoundError([ip])

        return geolocation
    }

    /**
     * Returns the geolocation of all the IPs in 
     *  a given array.
     *
     * @param {string[]} ips The array of ip to get the geolocations
     * @returns {Country[]}
     */
    getMultiple(ips: string[]) {

        // Validates if the array contains at least 1 element before proceeding
        if (Joi.array().min(1).validate(ips).error) {
            throw new InvalidIpsError()
        }

        let invalidIps: string[] = []
        let geolocationsNotFound: string[] = []
        let geolocations: Country[] = []

        for (let i = 0; i < ips.length; i++) {
            const ip = ips[i]

            // Build an array of the invalid IPs
            if (Joi.string().ip().validate(ip).error) {
                invalidIps.push(ip)
                continue
            }

            const geolocation = this._geolocationsRepository.get(ip)

            // Build the array of ips which geolocation's couldn't be found
            if (!geolocation) {
                geolocationsNotFound.push(ip)
                continue
            }

            // Build the array of geolocations
            geolocations.push(geolocation)
        }

        if (geolocationsNotFound.length)
            throw new GeolocationsNotFoundError(geolocationsNotFound)

        if (invalidIps.length)
            throw new InvalidIpsError(invalidIps)

        return geolocations
    }
}
