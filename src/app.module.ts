import { Module, MiddlewareConsumer } from '@nestjs/common';
import { GeolocationsModule } from './geolocations/geolocations.module';
import { ConfigModule } from 'nestjs-dotenv'

@Module({
    imports: [
        ConfigModule.forRoot(`environment/${process.env.NODE_ENV || 'development'}.env`),
        GeolocationsModule
    ],
    controllers: [],
    providers: [
        GeolocationsModule
    ],
})
export class AppModule {
}
