/* istanbul ignore file */
import { createParamDecorator } from '@nestjs/common';

import * as requestIp from 'request-ip';

/**
 * Custom Decorator
 * Retrieves and returns the IP address from the request
 */
export const IpAddress = createParamDecorator((data, req) => {
    if (req.clientIp)
        return req.clientIp
    return requestIp.getClientIp(req)
});