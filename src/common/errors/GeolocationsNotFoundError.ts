/**
 * This custom error is thrown be one or multiple 
 *  IP addresses can not be found.
 *
 * @export
 * @class GeolocationsNotFoundError
 * @extends {Error}
 */
export class GeolocationsNotFoundError extends Error {

    constructor(geolocationIps: string[]) {
        super()
        this.message = `Not found ips: ${geolocationIps.join(', ')}`
    }
}