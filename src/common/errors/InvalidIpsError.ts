/**
 * This custom error is thrown when one or multiple
 *  IP addresses or invalid.
 *
 * @export
 * @class InvalidIpsError
 * @extends {Error}
 */
export class InvalidIpsError extends Error {

    constructor(invalidIps?: string[]) {
        super()
        this.message = 'Invalid IPs'
        if (invalidIps)
            this.message += ` : ${invalidIps.join(', ')}`
    }
}